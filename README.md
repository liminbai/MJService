# MJService

#### 介绍
基于《幼麟棋牌-四川麻将v1.0》客户端，按照最新的TypeScript，TypeORM，Nodejs，Express改写的后端服务平台；

目前还处于开发阶段，吸收原始版本功能，按照新的结构进行了调整。总体思路：

![输入图片说明](https://images.gitee.com/uploads/images/2019/0901/130815_02590b66_638966.png "微信图片_20190901130750.png")

#### 软件架构

API接口采用： Express
持久层采用： TypeORM

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)