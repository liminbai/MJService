﻿import "reflect-metadata";
import * as configs from '../config';
import * as client_server from './client_server';
import { createConnection } from "typeorm";

createConnection().then(async () => {

    client_server.start(configs.client_server());

}).catch(error => console.log(error));