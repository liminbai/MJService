﻿/*
 * GET home page.
 */
import { Request, Response, Router } from "express";
import { getRepository } from "typeorm";
import { md5 } from "../../util/crypto";
import { t_users } from "../../entity/t_users";

const router = Router();

// 用户注册
router.get('/register', (res: Response) => {
    res.send('index');
});

// 用户注册
router.get('/index', (res: Response) => {
	
	//const account = req.body.account;
	//const password = req.body.password;
	
    res.send('index');
});

// 获得系统版本
router.get('/get_version', (res: Response) => {
	const ret = {
		version:this.appObj.locals['CONFIG_INFO'].VERSION,
	}
	res.send(ret);
});

// 获得大厅服务
router.get('/get_serverinfo',(res: Response) => {
	var ret = {
	 	version:this.appObj.locals['CONFIG_INFO'].VERSION,
	 	hall:this.appObj.locals['CONFIG_INFO'].HALL_IP + ":" + this.appObj.locals['CONFIG_INFO'].HAL_PORT,
	 	appweb:this.appObj.locals['CONFIG_INFO'].APP_WEB,
	}
	res.send(ret);
});

// 游客登陆
router.get('/guest',(req: Request, res: Response) => {
	var account = "guest_" + req.query.account;
	var sign = md5(account + req.ip + this.appObj.locals['CONFIG_INFO'].ACCOUNT_PRI_KEY);
	var ret = {
		errcode:0,
		errmsg:"ok",
		account:account,
		halladdr:this.appObj.locals['CONFIG_INFO'].HALL_IP + ":" + this.appObj.locals['CONFIG_INFO'].HALL_PORT,
		sign:sign
	}
	res.send(ret);
});

// 获得用户基本信息
router.get('/base_info',async (req: Request, res: Response) =>{
	var userid = req.query.userid;

	const users = await getRepository(t_users).createQueryBuilder()
	.where("userid = :userid", { userid: userid }).getOne();
	res.send(users);
	
});

export default function(app:any){
    this.appObj = app;
    return router;
};