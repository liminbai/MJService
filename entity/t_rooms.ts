import {Column,Entity,Index} from "typeorm";


@Entity("t_rooms",{schema:"db_mj" } )
@Index("uuid",["uuid",],{unique:true})
@Index("id",["id",],{unique:true})
export class t_rooms {

    @Column("char",{ 
        nullable:false,
        primary:true,
        length:20,
        name:"uuid"
        })
    uuid:string;
        

    @Column("char",{ 
        nullable:false,
        unique: true,
        length:8,
        name:"id"
        })
    id:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:256,
        default: () => "'0'",
        name:"base_info"
        })
    base_info:string;
        

    @Column("int",{ 
        nullable:false,
        name:"create_time"
        })
    create_time:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"num_of_turns"
        })
    num_of_turns:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"next_button"
        })
    next_button:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"user_id0"
        })
    user_id0:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:128,
        name:"user_icon0"
        })
    user_icon0:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:32,
        name:"user_name0"
        })
    user_name0:string;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"user_score0"
        })
    user_score0:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"user_id1"
        })
    user_id1:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:128,
        name:"user_icon1"
        })
    user_icon1:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:32,
        name:"user_name1"
        })
    user_name1:string;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"user_score1"
        })
    user_score1:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"user_id2"
        })
    user_id2:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:128,
        name:"user_icon2"
        })
    user_icon2:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:32,
        name:"user_name2"
        })
    user_name2:string;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"user_score2"
        })
    user_score2:number;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"user_id3"
        })
    user_id3:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:128,
        name:"user_icon3"
        })
    user_icon3:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:32,
        name:"user_name3"
        })
    user_name3:string;
        

    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"user_score3"
        })
    user_score3:number;
        

    @Column("varchar",{ 
        nullable:true,
        length:16,
        name:"ip"
        })
    ip:string | null;
        

    @Column("int",{ 
        nullable:true,
        default: () => "'0'",
        name:"port"
        })
    port:number | null;
        
}
