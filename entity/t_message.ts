import {Column,Entity} from "typeorm";


@Entity("t_message",{schema:"db_mj" } )
export class t_message {

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        length:32,
        name:"type"
        })
    type:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:1024,
        name:"msg"
        })
    msg:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:32,
        name:"version"
        })
    version:string;
        
}
