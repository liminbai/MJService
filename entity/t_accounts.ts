import {Column,Entity} from "typeorm";


@Entity("t_accounts",{schema:"db_mj" } )
export class t_accounts {

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        name:"account"
        })
    account:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"password"
        })
    password:string;
        
}
