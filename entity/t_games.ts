import {Column,Entity} from "typeorm";


@Entity("t_games",{schema:"db_mj" } )
export class t_games {

    @Column("char",{ 
        nullable:false,
        primary:true,
        length:20,
        name:"room_uuid"
        })
    room_uuid:string;
        

    @Column("smallint",{ 
        nullable:false,
        primary:true,
        name:"game_index"
        })
    game_index:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:1024,
        name:"base_info"
        })
    base_info:string;
        

    @Column("int",{ 
        nullable:false,
        name:"create_time"
        })
    create_time:number;
        

    @Column("char",{ 
        nullable:true,
        length:255,
        name:"snapshots"
        })
    snapshots:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:2048,
        name:"action_records"
        })
    action_records:string | null;
        

    @Column("char",{ 
        nullable:true,
        length:255,
        name:"result"
        })
    result:string | null;
        
}
