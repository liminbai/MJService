import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";


@Entity("t_users", { schema: "db_mj" })
@Index("account", ["account",], { unique: true })
export class t_users {

    @PrimaryGeneratedColumn({ type: "int", name: "userid" })
    userid: number;


    @Column("varchar", {nullable: false,unique: true,length: 64,name: "account"})
    account: string;


    @Column("varchar", {
        nullable: true,
        length: 32,
        name: "name"
    })
    name: string | null;


    @Column("int", {
        nullable: true,
        name: "sex"
    })
    sex: number | null;


    @Column("varchar", {
        nullable: true,
        length: 256,
        name: "headimg"
    })
    headimg: string | null;


    @Column("smallint", {
        nullable: true,
        default: () => "'1'",
        name: "lv"
    })
    lv: number | null;


    @Column("int", {
        nullable: true,
        default: () => "'0'",
        name: "exp"
    })
    exp: number | null;


    @Column("int", {
        nullable: true,
        default: () => "'0'",
        name: "coins"
    })
    coins: number | null;


    @Column("int", {
        nullable: true,
        default: () => "'0'",
        name: "gems"
    })
    gems: number | null;


    @Column("varchar", {
        nullable: true,
        length: 8,
        name: "roomid"
    })
    roomid: string | null;


    @Column("varchar", {
        nullable: false,
        length: 4096,
        name: "history"
    })
    history: string;

}
