import {Column,Entity} from "typeorm";


@Entity("t_guests",{schema:"db_mj" } )
export class t_guests {

    @Column("varchar",{ 
        nullable:false,
        primary:true,
        name:"guest_account"
        })
    guest_account:string;
        
}
