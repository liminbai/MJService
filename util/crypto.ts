import * as crypto from 'crypto';

const md5 = function (content) {
	var md5 = crypto.createHash('md5');
	md5.update(content);
	return md5.digest('hex');	
}

const toBase64 = function(content){
	return new Buffer(content).toString('base64');
}

const fromBase64 = function(content){
	return new Buffer(content, 'base64').toString();
}

export { md5, toBase64, fromBase64}