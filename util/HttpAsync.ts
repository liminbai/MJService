import * as request from "request";

export class HttpAsync{

    /**
     * 定义同步get
     * @param url 
     */
    public async getAsync(url:string):Promise<any>{
        let promise =  new Promise<any>(resolve=>{
            request.get(url,(error, response, body)=>{
                if(!error && response.statusCode == 200){
                    resolve(body);
                }else{
                    console.log(error);
                }
                
            })
        })
        return promise;
    }
}