﻿import "reflect-metadata";
import * as configs from '../config';
import * as game_server from './game_server';
import { createConnection } from "typeorm";

createConnection().then(async () => {

    game_server.start(configs.game_server());

}).catch(error => console.log(error));