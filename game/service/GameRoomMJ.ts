import { t_rooms } from "../../entity/t_rooms";
import { GameRoomService } from "./GameRoomService";

export class GameRoomMJ{

    static DI_FEN = [1,2,5];
    static MAX_FAN = [3,4,5];
    static JU_SHU = [4,8];
    static JU_SHU_COST = [2,3];
    static roomsLocation = {};
    static creatingRooms = {};
    static userLocation = {};
    static totalRooms = 0;

    /**
     * 创建房间
     * @param userId 创建人
     * @param userConf 用户房间配置
     * @param gems 钻石
     * @param ip 游戏服务IP地址
     * @param port 游戏服务端口
     */
    public static async createRoom(userId:string,userConf:any,gems:number,ip:string,port:number) {
        if(
            userConf.type == null
            || userConf.difen == null
            || userConf.zimo == null
            || userConf.jiangdui == null
            || userConf.huansanzhang == null
            || userConf.zuidafanshu == null
            || userConf.jushuxuanze == null
            || userConf.dianganghua == null
            || userConf.menqing == null
            || userConf.tiandihu == null){
            return;
        }
    
        if(userConf.difen < 0 || userConf.difen > this.DI_FEN.length){
            return;
        }
        if(userConf.zimo < 0 || userConf.zimo > 2){
            return;
        }
        if(userConf.zuidafanshu < 0 || userConf.zuidafanshu > this.MAX_FAN.length){
            return;
        }
        if(userConf.jushuxuanze < 0 || userConf.jushuxuanze > this.JU_SHU.length){
            return;
        }
        var cost = this.JU_SHU_COST[userConf.jushuxuanze];
        if(cost > gems){
            return;
        }

        // 生成RoomID
        const roomId = '' + this.generateRoomId();
        
        this.creatingRooms[roomId] = true;

        const room = new t_rooms();
        room.uuid = Date.now() + roomId;
        room.id = roomId;
        room.ip = ip;
        room.port = port;
        room.create_time = Math.ceil(Date.now()/1000);
        room.base_info = JSON.stringify({
            type:userConf.type,
            baseScore:this.DI_FEN[userConf.difen],
            zimo:userConf.zimo,
            jiangdui:userConf.jiangdui,
            hsz:userConf.huansanzhang,
            dianganghua:parseInt(userConf.dianganghua),
            menqing:userConf.menqing,
            tiandihu:userConf.tiandihu,
            maxFan:this.MAX_FAN[userConf.zuidafanshu],
            maxGames:this.JU_SHU[userConf.jushuxuanze],
            creator:userId
        });

        // 数据库创建成功
        new GameRoomService().save_room(room).then((val:t_rooms)=>{
            if(val){
                // 创建本地rooms
                delete this.creatingRooms[roomId];
                room.uuid = val.uuid;
                this.roomsLocation[roomId] = this.initLocalRoom(userId,userConf,room);
                this.totalRooms++;
    
                return roomId;
            }else{
                return null;
            }
        });
    }

    /**
     * 用户加入房间
     * @param roomId 
     * @param userId 
     * @param userName 
     */
    public static enterRoom(userId:string,userName:string,roomId:string){

        const lroom = this.roomsLocation[roomId];
        // 如果本地房间存在,检查和更新座位
        if(lroom){
            return this.updateRoomSeat(userId,roomId,userName,lroom);
        }else{
            new GameRoomService().search_room_id(roomId).then((val:t_rooms)=>{
                // 如果数据库房间存在，则通过数据库初始化本地房间，同时检查和更新座位
                if(val){
                    const room = this.initRoomFromDb(val);
                    this.roomsLocation[roomId] = room;
                    this.totalRooms++;
                    return this.updateRoomSeat(userId,roomId,userName,room);
                }else{
                    return 2;
                }
            });
        }
    }

    /**
     * 更新房间座位信息
     * @param userId 
     * @param roomId 
     * @param userName 
     * @param room 
     */
    public static updateRoomSeat(userId,roomId,userName,room):number{

        // 判断用户是否已经在当前房间
        if(this.getUserRoom(userId) == roomId){
            return 0;
        }else{
            for(let i = 0; i < 4; ++i) {
                const seat = room.seats[i];
                // 如果房间座位为0,安排用户进入
                if(seat['userId'] <= 0) {
                    seat['userId'] = userId;
                    seat['name'] = userName;
                    this.userLocation[userId] = {
                        roomId:roomId,
                        seatIndex:i
                    };
                    //console.log(userLocation[userId]);
                    // 更新数据库座位
                    new GameRoomService().update_room_seat(roomId,i,seat.userId,"",seat.name);
                    //正常
                    return 0;
                }
            }
            //房间已满
            return 1;
        }
    }

    /**
     * 通过用户提交信息，初始化房间
     * @param userId 
     * @param roomId 
     * @param userConf 
     */
    private static initLocalRoom(userId,userConf,room:t_rooms){
        // 初始化房间信息
        const roomInfo = {
            uuid:room.uuid,
            id:room.id,
            numOfGames:0,
            createTime:room.create_time,
            nextButton:0,
            seats:[],
            conf:{
                type:userConf.type,
                baseScore:this.DI_FEN[userConf.difen],
                zimo:userConf.zimo,
                jiangdui:userConf.jiangdui,
                hsz:userConf.huansanzhang,
                dianganghua:parseInt(userConf.dianganghua),
                menqing:userConf.menqing,
                tiandihu:userConf.tiandihu,
                maxFan:this.MAX_FAN[userConf.zuidafanshu],
                maxGames:this.JU_SHU[userConf.jushuxuanze],
                creator:userId,
            }
        };
        
        // 根据用户选择加载游戏
        // if(userConf.type == "xlch"){
        //     roomInfo.gameMgr = require("./gamemgr_xlch");
        // }else{
        //     roomInfo.gameMgr = require("./gamemgr_xzdd");
        // }

        console.log(roomInfo.conf);
        
        // 初始化座位信息
        for(var i = 0; i < 4; ++i){
            roomInfo.seats.push({
                userId:0,
                score:0,
                name:"",
                ready:false,
                seatIndex:i,
                numZiMo:0,
                numJiePao:0,
                numDianPao:0,
                numAnGang:0,
                numMingGang:0,
                numChaJiao:0,
            });
        }

        return roomInfo;
    }

    /**
     * 通过数据库信息，初始化房间
     * @param dbdata 
     */
    private static initRoomFromDb(dbroom:t_rooms){

        const roomInfo = {
            uuid:dbroom.uuid,
            id:dbroom.id,
            numOfGames:dbroom.num_of_turns,
            createTime:dbroom.create_time,
            nextButton:dbroom.next_button,
            seats:new Array(4),
            conf:JSON.parse(dbroom.base_info)
        };
    
        // if(roomInfo.conf.type == "xlch"){
        //     roomInfo.gameMgr = require("./gamemgr_xlch");
        // }else{
        //     roomInfo.gameMgr = require("./gamemgr_xzdd");
        // }

        const roomId = roomInfo.id;
        
        // 初始化4个座位信息
        for(let i = 0; i < 4; ++i){
            const s = roomInfo.seats[i] = {};
            s['userId'] = dbroom["user_id" + i];
            s['score'] = dbroom["user_score" + i];
            s['name'] = dbroom["user_name" + i];
            s['ready'] = false;
            s['seatIndex'] = i;
            s['numZiMo'] = 0;
            s['numJiePao'] = 0;
            s['numDianPao'] = 0;
            s['numAnGang'] = 0;
            s['numMingGang'] = 0;
            s['numChaJiao'] = 0;
    
            if(s['userId'] > 0){
                this.userLocation[s['userId']] = {
                    roomId:roomId,
                    seatIndex:i
                };
            }
        }

        return roomInfo;
    }
    
    /**
     * 通过本地获取用户房间ID
     * @param userId 
     */
    private static getUserRoom(userId:string){
        const location = this.userLocation[userId];
        if(location != null){
            return location.roomId;
        }
        return null;
    };
    
    // 获得游戏房间ID
    private static generateRoomId(){
        let roomId:string = '';
        for(let i = 0; i < 6; ++i){
            roomId += Math.floor(Math.random()*10);
        }
        // 如果本地和待创建都存在，重新获取ID
        if(this.roomsLocation[roomId] != null || this.creatingRooms[roomId] != null){
            this.generateRoomId(); 
        }

        // 查询roomID是否存在
        const room = new GameRoomService().search_room_id(roomId);
        // 如果数据库已经存在，清除待创建，重新获取ID
        if(room){
            delete this.creatingRooms[roomId];
            this.generateRoomId(); 
        }
        return roomId;
    }
}