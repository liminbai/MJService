import { getRepository } from "typeorm";
import { t_rooms } from "../../entity/t_rooms";
import { toBase64,fromBase64 } from "../../util/crypto";

export class GameRoomService{

    /**
     * 通过房间ID查询房间信息
     * @param roomId 
     */
    public async search_room_id(roomId:string):Promise<t_rooms>{
        // 查询roomID是否存在
        return await getRepository(t_rooms).createQueryBuilder()
        .where("id = :id", { id: roomId }).getOne()
        .then((val:t_rooms)=>{
            val.user_name0 = fromBase64(val.user_name0);
            val.user_name1 = fromBase64(val.user_name1);
            val.user_name2 = fromBase64(val.user_name2);
            val.user_name3 = fromBase64(val.user_name3);
            return val;
        });    
    }

    /**
     * 保持房间信息
     * @param room 
     */
    public async save_room(room:t_rooms):Promise<t_rooms>{
        return await getRepository(t_rooms).save(room);   
    }

    /**
     * 更新用户座位信息
     * @param roomId 
     * @param seatIndex 
     * @param userId 
     * @param icon 
     * @param name 
     */
    public async update_room_seat(roomId:string,seatIndex:number,userId:string,icon:string,name:string){
        
        const roomRepository = await getRepository(t_rooms);
        roomRepository.findOne({ id: roomId }).then(async (val:t_rooms) => {
            switch(seatIndex){
                case 0: { 
                    val.user_id0 = parseInt(userId);
                    val.user_icon0 = toBase64(icon);
                    val.user_name0 = name;
                    break; 
                }
                case 1: { 
                    val.user_id1 = parseInt(userId);
                    val.user_icon1 = toBase64(icon);
                    val.user_name1 = name;
                    break; 
                } 
                case 2: { 
                    val.user_id2 = parseInt(userId);
                    val.user_icon2 = toBase64(icon);
                    val.user_name2 = name;
                    break; 
                } 
                case 3: { 
                    val.user_id3 = parseInt(userId);
                    val.user_icon3 = toBase64(icon);
                    val.user_name3 = name;
                    break; 
                } 
            }
            roomRepository.save(val);
        });
    }
}