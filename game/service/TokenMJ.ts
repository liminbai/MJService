import { md5 } from '../../util/crypto';

export class TokenMJ{
    private static tokens = {};
    private static users = {};

    public static createToken(userId:string,lifeTime:number){
        let token = this.users[userId];
        if(token != null){
            this.delToken(token);
        }

        const time = Date.now();
        token = md5(userId + "!@#$%^&" + time);
        this.tokens[token] = {
            userId: userId,
            time: time,
            lifeTime: lifeTime
        };
        this.users[userId] = token;
        return token;
    }

    public static getToken(userId:string){
        return this.users[userId];
    };

    public static getUserID(token:string){
        return this.tokens[token].userId;
    };

    public static isTokenValid(token:string){
        const info = this.tokens[token];
        if(info == null){
            return false;
        }
        if(info.time + info.lifetime < Date.now()){
            return false;
        }
        return true;
    };

    public static delToken(token:string){
        const info = this.tokens[token];
        if(info != null){
            this.tokens[token] = null;
            this.users[info.userId] = null;
        }
    };
}