﻿
import { Request, Response, Router } from "express";
import { md5 } from "../../util/crypto";
import { GameRoomMJ } from "../service/GameRoomMJ";
import { TokenMJ } from "../service/TokenMJ";

const router = Router();

// 获得游戏服务器信息
router.get('/get_server_info', (req: Request, res: Response) =>{
	const serverId = req.query.serverid;
	const sign = req.query.sign;
	
	// 判断服务器ID验证是否正确
	if(serverId != this.appObj.locals['CONFIG_INFO'].SERVER_ID || sign == null){
		res.send({"errcode":1,"errmsg":"invalid parameters"});
		return;
	}

	// 判断服务器加密串是否正确
	const md = md5(serverId + this.appObj.locals['CONFIG_INFO'].HALL_PRI_KEY);
	if(md != sign){
		res.send({"errcode":1,"errmsg":"sign check failed."});
		return;
	}

	console.log(serverId);
	console.log(sign,md);

	var locations = {};//roomMgr.getUserLocations()
	var arr = [];
	for(var userId in locations){
		var roomId = locations[userId].roomId;
		arr.push(userId);
		arr.push(roomId);
	}
	res.send({"errcode":0,"errmsg":"ok",userroominfo:arr});
});

// 创建游戏房间
router.get('/create_room', (req: Request, res: Response) =>{
	const userId = req.query.userid;
	const sign = req.query.sign;
	const gems = req.query.gems;
	const conf = req.query.conf

	if(userId == null || sign == null || conf == null){
		res.send({"errcode":1,"errmsg":"invalid parameters"});
		return;
	}

	console.log('用户ID：',userId);
	console.log('用户配置：',conf);
	console.log('用户钻石：',gems);
	console.log('访问key：',this.appObj.locals['CONFIG_INFO'].HALL_PRI_KEY);

	//访问验证
	const md = md5(userId + conf + gems + this.appObj.locals['CONFIG_INFO'].HALL_PRI_KEY);
	if(md != sign){
		console.log("invalid reuqest.");
		res.send({"errcode":1,"errmsg":"sign check failed."});
		return;
	}

	//创建房间，并返回ID
	const roomId = GameRoomMJ.createRoom(userId,JSON.parse(conf),gems,this.appObj.locals['CONFIG_INFO'].GAME_IP,this.appObj.locals['CONFIG_INFO'].SOCKET_PORT);
	if(roomId){
		res.send({"errcode":0,"errmsg":"ok",roomid:roomId});
	}else{
		res.send({"errcode":1,"errmsg":"create failed."});
	}
});

// 进入游戏房间
router.get('/enter_room', (req: Request, res: Response) =>{
	const userId = req.query.userid;
	const name = req.query.name;
	const roomId = req.query.roomid;
	const sign = req.query.sign;
	if(userId == null || roomId == null || sign == null){
		res.send({"errcode":1,"errmsg":"invalid parameters."});
		return;
	}

	const md = md5(userId + name + roomId + this.appObj.locals['CONFIG_INFO'].HALL_PRI_KEY);
	console.log(req.query);
	console.log(md5);
	if(md != sign){
		res.send({"errcode":2,"errmsg":"sign check failed."});
		return;
	}

	//安排玩家坐下
	const state = GameRoomMJ.enterRoom(roomId,userId,name);
	//正常，返回用户ID
	if(state == 0){
		const token = TokenMJ.createToken(userId,5000);
		res.send({"errcode":0,"errmsg":"ok",token:token});
	}else if(state == 1){
		res.send({"errcode":4,"errmsg":"room is full."});
	}else if(state == 2){
		res.send({"errcode":3,"errmsg":"can't find room."});
	}
});

// 判断游戏房间是否运行
router.get('/is_room_runing', (req: Request, res: Response) =>{
	const roomId = req.query.roomid;
	const sign = req.query.sign;
	if(roomId == null || sign == null){
		res.send({"errcode":1,"errmsg":"invalid parameters."});
		return;
	}

	var md = md5(roomId + this.appObj.locals['CONFIG_INFO'].HALL_PRI_KEY);
	if(md != sign){
		res.send({"errcode":2,"errmsg":"sign check failed."});
		return;
	}
	
	//var roomInfo = roomMgr.getRoom(roomId);
	res.send({"errcode":0,"errmsg":"ok",runing:true});
});

export default function(app:any){
    this.appObj = app;
    return router;
};