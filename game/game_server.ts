﻿import * as express from "express";
import * as HttpClient from "request";
import * as querystring from "querystring";

import game from './routes/game';

var app = express();

// 跨域访问
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By",' 3.2.1')
    if(req.method=="OPTIONS") res.send(200);/*让options请求快速返回*/
    else  next();
});

// 加载路由
app.use('/', game(app));


// catch 404 and forward to error handler
app.use(function ({}, {}, next) {
    const err = new Error('Not Found');
    err['status'] = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use((err: any, {}, res, {}) => {
        res.status(err['status'] || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use((err: any, {}, res, {}) => {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

// 校验大厅心跳服务
function check_hall(){
    let lastTickTime = 0;

    if(lastTickTime + app.locals['CONFIG_INFO'].GAME_TICK_TIME < Date.now()){
        lastTickTime = Date.now();
        
        let gameServerInfo = {
            id:app.locals['CONFIG_INFO'].SERVER_ID,
            hallIp:app.locals['CONFIG_INFO'].HALL_IP,
            hallPort:app.locals['CONFIG_INFO'].HALL_PORT,
            gamePort:app.locals['CONFIG_INFO'].GAME_PORT,
            load:0,
        };
        //load: roomMgr.getTotalRooms()

        //请求大厅地址
        let url = 'http://'+app.locals['CONFIG_INFO'].HALL_IP+':'+app.locals['CONFIG_INFO'].HALL_PORT+'/register_gs';
        url += '?' + querystring.stringify(gameServerInfo);
        
        console.log('请求大厅地址:',url);

        HttpClient.get(url,function(error, response, body){
            if(!error && response.statusCode == 200){
                const data = JSON.parse(body);
                console.log(data);
                if(data.errcode != 0){
					console.log(data.errmsg);
				}
                if(data.gameIp != null){
					console.log(data.gameIp);
				}
            }else{
                console.log(error);
            }
        });
		
		var mem = process.memoryUsage();
		var format = function(bytes) {  
              return (bytes/1024/1024).toFixed(2)+'MB';  
        }; 
		console.log('Process: heapTotal '+format(mem.heapTotal) + ' heapUsed ' + format(mem.heapUsed) + ' rss ' + format(mem.rss));
	}
}

// 启动服务
export function start(cfg:any) {
    app.set('port', cfg.GAME_PORT);
    app.locals['CONFIG_INFO'] = cfg;

    setInterval(check_hall,5000);

    const server = app.listen(app.get('port'), '0.0.0.0', function () {
        const host = server.address().address;
        const port = server.address().port;
        console.log('游戏服务启动,地址为 http://%s:%s',host,port);
    });
}


