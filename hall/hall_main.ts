﻿
import "reflect-metadata";
import * as configs from '../config';
import * as hall_server from './hall_server';
import { createConnection } from "typeorm";

createConnection().then(async () => {

    hall_server.start(configs.hall_server());

}).catch(error => console.log(error));
