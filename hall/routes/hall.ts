﻿/*
 * GET home page.
 */

import { Request, Response, Router } from "express";
import { t_users } from "../../entity/t_users";
import { toBase64,md5 } from "../../util/crypto";
import { RemoteGameService } from "../service/RemoteGameService";
import { HallService } from "../service/HallService";

const router = Router();
const serverMap = {};
const hallService = new HallService();

// 登陆游戏大厅
router.get('/login', async (req: Request, res: Response) => {
	if(!check_account(req,res)){
		return;
	}

	const ip = getIP(req);
	const account = req.query.account;

	// 查询用户
	const user = await hallService.get_user_data(account);
	
	const ret = {
		account:user.account,
		userid:user.userid,
		name:user.name,
		lv:user.lv,
		exp:user.exp,
		coins:user.coins,
		gems:user.gems,
		ip:ip,
		sex:user.sex
	};

	if(user == null){

		res.send({"errcode":0,"errmsg":"ok"});

	}else if(user.roomid != null){//如果用户处于房间中，则需要对其房间进行检查。 如果房间还在，则通知用户进入
		
		//检查房间是否存在于数据库中
		const room = await hallService.get_room_data(user.roomid);
		if(room){
			ret['roomid'] = room.uuid;
		}else{
			//如果房间不在了，表示信息不同步，清除掉用户记录
			await hallService.set_room_id_of_user(user.userid,null);
		}

		ret['errcode'] = 0;
		ret['errmsg'] = "ok";
		res.send(ret);

	}else{
		ret['errcode'] = 0;
		ret['errmsg'] = "ok";
		res.send(ret);
	}
});

// 创建用户
router.get('/create_user', async (req: Request, res: Response) =>{
	if(!check_account(req,res)){
		return;
	}
	const account = req.query.account;
	const name = req.query.name;
	const coins = 1000;
	//const gems = 21;
	console.log(name);

	// 查询用户
	const user = await hallService.get_user_data(account);

	if(user == null){
		const user = new t_users();
		user.userid = generateUserId();
		user.account = account;
		user.name = toBase64(name);
		user.coins = coins;
		user.sex = 0;
		// 创建用户
		await hallService.create_user(user);
		
		res.send({"errcode":0,"errmsg":"ok"});
	}else{
		res.send({"errcode":1,"errmsg":"account have already exist."});
	}
});

// 获取用户状态
router.get('/get_user_status',async (req: Request, res: Response) =>{
	if(!check_account(req,res)){
		return;
	}
	const account = req.query.account;

	const user = await hallService.get_user_data(account);

	if(user != null){
		res.send({"errcode":0,"errmsg":"ok","gems":user.gems});
	}else{
		res.send({"errcode":1,"errmsg":"get gems failed."});
	}
});

// 获取系统消息
router.get('/get_message',async (req: Request, res: Response) =>{
	if(!check_account(req,res)){
		return;
	}

	const type = req.query.type;
	
	if(type == null){
		res.send({"errcode":-1,"errmsg":"parameters don't match api requirements."});
		return;
	}

	//查询消息
	const message = await hallService.get_message(type);

	if(message != null){
		res.send({"errcode":0,"errmsg":"ok",'msg':message.msg,'version':message.version});	
	}else{
		res.send({"errcode":1,"errmsg":"get message failed."});
	}
	
});

// 创建房间
router.get('/create_private_room',async (req: Request, res: Response) => {
	//验证参数合法性
	const data = req.query;
	//验证玩家身份
	if(!check_account(req,res)){
		return;
	}

	const account = data.account;
	const user = await hallService.get_user_data(account);

	//判断玩家是否存在
	if(user == null){
		res.send({"errcode":-1,"errmsg":"system error"});
		return;
	}
	
	//判断玩家已经在其他房间
	if(user.roomid != null){
		res.send({"errcode":-1,"errmsg":"user is playing in room now."});
		return;
	}

	// 获得游戏服务器地址
	const serverinfo = chooseServer();

	const remoteGameService = new RemoteGameService(serverinfo);

	console.log('服务器信息：',remoteGameService);

	const key = md5(user.userid + data.conf + user.gems + this.appObj.locals['CONFIG_INFO'].HALL_PRI_KEY);
	
	// 创建远程游戏服务器房间
	const body = await remoteGameService.createGameRoom(user.userid,user.gems,data.conf,key);
	
	//创建成功进入房间
	if (body.errcode == 0 && body.roomid != null) {
		
		const body2 = this.enterRoom(user.userid,user.name,body.roomid);

		if (body2 && body2.errcode == 0) {

			body2['roomid'] = body.roomid;
			body2['time'] = Date.now();
			body2['sign'] = md5(body.roomid + body2.token + body2.time + this.appObj.locals['CONFIG_INFO'].HALL_PRI_KEY);

			res.send(body2);
		}else{
			res.send(body2);
		}
	} else {
		console.log(body);
	}
});

//加入指定房间
router.get('/enter_private_room',async (req: Request, res: Response) =>{
	const data = req.query;
	const roomId = data.roomid;

	if(roomId == null){
		res.send({"errcode":-1,"errmsg":"parameters don't match api requirements."});
		return;
	}
	if(!check_account(req,res)){
		return;
	}

	const user = await hallService.get_user_data(data.account);
	// 如果用户存在进入房间
	if(user){
		//创建成功进入房间
		const body = enterRoom(user.userid,user.name,roomId);

		if (body && body['errcode'] == 0) {
			// 更新用户房间号
			await hallService.set_room_id_of_user(user.userid,roomId);

			body['roomid'] = roomId;
			body['time'] = Date.now();
			body['sign'] = md5(roomId + body['token'] + body['time'] + this.appObj.locals['CONFIG_INFO'].HALL_PRI_KEY);

			res.send(body);
		}else{
			res.send(body);
		}
	}else{
		res.send({"errcode":-1,"errmsg":"system error"});
	}
	
});

// 游戏服注册
router.get('/register_gs', (req: Request, res: Response) =>{
	
	const gameIp = getIP(req);//game服务器请求地址
	const gamePort = req.query.gamePort;//game服务器端口
	const hallIp = req.query.hallIp;
	const hallPort = req.query.hallPort;
	const load = req.query.load;
	const id = gameIp + "_" + gamePort;

	// 判断服务器信息是否存在
	if(serverMap[id]){
		const info = serverMap[id];
		if(info.hallPort != hallPort || info.gamePort != gamePort || info.gameIp != gameIp){
			console.log("duplicate gsid:" + id + ",gameAddr:" + gameIp + "(" + gamePort + ")");
			res.send({"errcode":1,"errmsg":"duplicate gsid:" + id});
			return;
		}
		info.load = load;
		res.send({"errcode":0,"errmsg":"ok","gameIp":gameIp});
	}else{
		// 初始化游戏服务器信息
		serverMap[id] = {
			id:id,
			gameIp:gameIp,
			gamePort:gamePort,
			hallIp:hallIp,
			hallPort:hallPort,
			load:load
		};

		// 获取远程服务器信息
		const remoteGameService = new RemoteGameService(serverMap[id]);
		const key = md5(id + this.appObj.locals['CONFIG_INFO'].HALL_PRI_KEY);
		remoteGameService.getGameServiceInfo(id,key);
		
		// 打印服务器性能
		console.log("game server registered.\n\tid:" + id + "\n\tgame addr:" + gameIp + "\n\tgame port:" + gamePort + "\n\tsocket hallPort:" + hallPort);
		
		res.send({"errcode":0,"errmsg":"ok","gameIp":gameIp});
	}

});

/**
 * 进入游戏房间
 * @param userId 
 * @param name 
 * @param roomId 
 */
async function enterRoom(userId:number,name:string,roomId:string){

	//获取房间信息
	const room = await hallService.get_room_data(roomId);
	
	// 判断房间是否存在
	if(room){
		const id = room.ip + "_" + room.port;
		let serverinfo = serverMap[id] != null ? serverMap[id] : chooseServer();

		// 判断服务器是否存在
		if(serverinfo){

			const remoteGameService = new RemoteGameService(serverinfo);
			// 判断服务器是否运行，如果运行请求进入房间
			let key = md5(roomId + this.appObj.locals['CONFIG_INFO'].HALL_PRI_KEY)
			const isrun = await remoteGameService.checkGameRoomIsRuning(roomId,key);

			if(isrun){
				key = md5(userId + name + roomId + this.appObj.locals['CONFIG_INFO'].HALL_PRI_KEY)
				// 进入房间请求
				const body = await remoteGameService.enterGameRoomReq(userId,name,roomId,key);
				// 请求成功
				if(body.errcode == 0){
					body['ip'] = serverinfo.gameIp;
					body['port'] = serverinfo.gamePort;

					// 更新用户房间号
					await hallService.set_room_id_of_user(userId,roomId);
				}

				return body;
			}

		}else{
			return null;
		}
	}else{
		return null;
	}
}




// 验证账号合法性
function check_account(req: Request, res: Response){
	var account = req.query.account;
	var sign = req.query.sign;
	if(account == null || sign == null){
		res.send({"errcode":1,"errmsg":"unknown error"});
		return false;
	}
	/*
	var serverSign = crypto.md5(account + req.ip + config.ACCOUNT_PRI_KEY);
	if(serverSign != sign){
		http.send(res,2,"login failed.");
		return false;
	}
	*/
	return true;
}

// 获取用户ID
function generateUserId() {
    let Id = 0;
    for (var i = 0; i < 6; ++i) {
        if (i > 0) {
            Id += Math.floor(Math.random() * 10);
        } else {
            Id += Math.floor(Math.random() * 9) + 1;
        }
    }
    return Id;
}

// 获得客户端访问IP
function getIP(req: Request){
	let ip = req.ip;
	if(ip.indexOf("::ffff:") != -1){
		ip = ip.substr(7);
	}
	return ip;
}

// 选择游戏服
function chooseServer(){
	let serverinfo = null;
	for(let s in serverMap){
		const info = serverMap[s];
		if(serverinfo == null){
			serverinfo = info;		
		}else{
			if(serverinfo.load > info.load){
				serverinfo = info;
			}
		}
	}	
	return serverinfo;
}

export default function(app:any){
    this.appObj = app;
    return router;
};