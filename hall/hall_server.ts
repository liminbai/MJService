﻿import * as express from "express";
import hall from './routes/hall';

var app = express();

// 跨域访问
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By",' 3.2.1')
    if(req.method=="OPTIONS") res.send(200);/*让options请求快速返回*/
    else  next();
});

// 加载客户路由
app.use('/', hall(app));


// catch 404 and forward to error handler
app.use(function ({}, {}, next) {
    const err = new Error('Not Found');
    err['status'] = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use((err: any, {}, res, {}) => {
        res.status(err['status'] || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use((err: any, {}, res, {}) => {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

// 启动服务
export function start(cfg:any) {
    app.set('port', cfg.HALL_PORT);
    app.locals['CONFIG_INFO'] = cfg;

    const server = app.listen(app.get('port'),'0.0.0.0', function () {
        const host = server.address().address;
        const port = server.address().port;
        console.log('大厅服务启动,地址为 http://%s:%s',host,port);
    });
}


