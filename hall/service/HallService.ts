import { t_users } from "../../entity/t_users";
import { getRepository, UpdateResult } from "typeorm";
import { t_rooms } from "../../entity/t_rooms";
import { t_message } from "../../entity/t_message";

export class HallService{

    /**
     * 通过账号查询用户信息
     * @param account 
     */
    public async get_user_data(account:string):Promise<t_users>{
        return await getRepository(t_users).createQueryBuilder()
        .where("account = :account", { account: account }).getOne();
    }

    /**
     * 更新userid指定的roomid
     * @param userId 
     * @param roomId
     */
    public async set_room_id_of_user(userId:number,roomId:string):Promise<UpdateResult>{
        return await getRepository(t_users).createQueryBuilder().update()
        .set({ roomid: roomId}).where("userid = :userid", { userid: userId }).execute();
    }

    /**
     * 创建用户
     * @param user 
     */
    public async create_user(user:t_users):Promise<t_users>{
        return await getRepository(t_users).save(user);
    }

    /**
     * 通过房间ID查询房间信息
     * @param roomId 
     */
    public async get_room_data(roomId:string):Promise<t_rooms>{
        return await getRepository(t_rooms).createQueryBuilder()
		.where("id = :id", { id: roomId }).getOne();
    }

    /**
     * 通过类型查询消息
     * @param type 
     */
    public async get_message(type:string):Promise<t_message>{
        return await getRepository(t_message).createQueryBuilder()
        .where("type = :type", { type: type }).getOne();
    }
}