import * as querystring from "querystring";
import { HttpAsync } from "../../util/HttpAsync";
import request = require("request");

export class RemoteGameService {

    private serverinfo: any;

    constructor(sinfo: any) {
        this.serverinfo = sinfo;
    }

    /**
     * 创建远程房间信息
     * @param userid 
     * @param gems 
     * @param userconf 
     * @param key 
     */
    public async createGameRoom(userId: number, gems: any, userconf: any, key: any): Promise<any> {
        // 通过游戏服务器创建游戏房间
        let url = 'http://' + this.serverinfo.gameIp + ':' + this.serverinfo.gamePort + '/create_room';
        url += '?' + querystring.stringify({
            userid: userId,
            gems: gems,
            conf: userconf,
            sign: key
        });

        const httpAsync = new HttpAsync();
        let body = await httpAsync.getAsync(url);
        body = JSON.parse(body);

        console.log('远程房间信息:', body);

        return body;
    }

    /**
     * 检查游戏服房间是否运行
     * @param roomId 
     */
    public async checkGameRoomIsRuning(roomId: string, key: any): Promise<boolean> {
        // 判断服务器是否运行
        let url = 'http://' + this.serverinfo.gameIp + ':' + this.serverinfo.gamePort + '/is_room_runing';
        url += '?' + querystring.stringify({
            roomid: roomId,
            sign: key
        });

        const httpAsync = new HttpAsync();
        let body = await httpAsync.getAsync(url);
        body = JSON.parse(body);

        if (body.errcode == 0 && body.runing == true) {
            console.log('判断服务器运行:', body);
            return true;
        } else {
            console.log(body.errmsg);
            return false;
        }
    }

    /**
     * 向游戏服发起进入房间请求
     * @param userId 
     * @param name 
     * @param roomId 
     */
    public async enterGameRoomReq(userId:number, name:string, roomId:string, key:any): Promise<any> {
        // 进入房间
        let url = 'http://' + this.serverinfo.gameIp + ':' + this.serverinfo.gamePort + '/enter_room';
        url += '?' + querystring.stringify({
            userid: userId,
            name: name,
            roomid: roomId,
            sign: key
        });

        const httpAsync = new HttpAsync();
        let body = await httpAsync.getAsync(url);
        body = JSON.parse(body);

        console.log('进入房间信息:', body);

        return body;
    }

    public getGameServiceInfo(id:string,key:any){
        //请求游戏服地址
		let url = 'http://' + this.serverinfo.gameIp +':'+ this.serverinfo.gamePort + '/get_server_info';
		url += '?' + querystring.stringify({
			serverid:id,
			sign:key
		});

		console.log('请求游戏服地址:' + url);

		//获取游戏服务器信息
		request.get(url,function(error, response, body){
			if(!error && response.statusCode == 200){
				const data = JSON.parse(body);
				console.log('游戏服务器信息:',data);
				if(data.errcode == 0){
					console.log(data.errmsg);
				}else{
					console.log(data.errmsg);
				}
			}else{
				console.log(error);
			}
		});
    }
}