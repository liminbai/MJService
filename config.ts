﻿const HALL_PRI_KEY = "~!@#$(*&^%$&";
const ACCOUNT_PRI_KEY = "^&*#$%()@";

// 客户端访问服务
function client_server() {
    return {
        CLIENT_IP: '127.0.0.1',
        CLIENT_PORT: 9000,
        HALL_IP: '127.0.0.1',
        HALL_PORT: 9001,
        ACCOUNT_PRI_KEY: ACCOUNT_PRI_KEY,
        VERSION: '20190803',
        APP_WEB:'http://fir.im/2f17'
    }
}

// 游戏大厅访问服务
function hall_server() {
    return {
        CLEINT_IP: '127.0.0.1',
        CLEINT_PORT: 9000,
        HALL_IP: '127.0.0.1',
        HALL_PORT: 9001,
        ROOM_IP:'127.0.0.1',
		ROOM_PORT:9002,
		ACCOUNT_PRI_KEY:ACCOUNT_PRI_KEY,
		HALL_PRI_KEY:HALL_PRI_KEY
    }
}

// 游戏服务
function game_server() {
    return {
        SERVER_ID:'127.0.0.1_'+9002,
        //游戏服务IP&端口
        GAME_IP: '127.0.0.1',
        GAME_PORT: 9002,
       
        //游戏TICK的间隔时间，用于向大厅服汇报情况
        GAME_TICK_TIME:5000,

        //大厅服IP&端口
        HALL_IP: '127.0.0.1',
        HALL_PORT: 9001,

        //与大厅服协商好的通信加密KEY
        HALL_PRI_KEY:HALL_PRI_KEY,
        
        SOCKET_PORT: 10000
    }
}

export { game_server,client_server,hall_server };